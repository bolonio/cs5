"use strict";

module.exports = function(sequelize, DataTypes) {
  var Lecturer = sequelize.define("Lecturer", {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    name: DataTypes.STRING,
    surname: DataTypes.STRING,
    email: DataTypes.STRING
  });
  return Lecturer;
};