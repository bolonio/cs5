var express = require('express');
var router  = express.Router();
var models  = require('../../models');

// Get all bookings
router.get('/', function(req, res) {
  models.Booking.findAll({ include: [{ all: true, nested: true }] })
  .then(function(bookings) {
    if (bookings)
      res.json(bookings);
    else
      res.status(401).send('Bookings not found');
  });
});

// Get one booking information by bookingid
router.get('/:bookingid',function(req,res){
  models.Building.findOne(
    {
      where: {id: req.params.subjectid},
      include: [{ all: true, nested: true }]
    }
  ).then(function(booking) {
    if (booking)
      res.json(booking);
    else
      res.status(401).send('Booking not found');
  });
});

module.exports = router;
