"use strict";

module.exports = function(sequelize, DataTypes) {
  var Booking = sequelize.define("Booking", {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    weekday: DataTypes.INTEGER,
    weeknumber: DataTypes.INTEGER
  });
  return Booking;
};