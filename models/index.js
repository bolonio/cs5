"use strict";

var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + '/../config/config.json')[env];
var sequelize = new Sequelize(config.database, config.username, config.password, config);
var db = {};

/*
sequelize.authenticate()
.then(function () {
		console.log('The connection with the database has been established successfully.');
})
.catch(function (err) {
		console.log('Unable to connect to the database:', err);
})
.done();
*/

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
  })
  .forEach(function(file) {
    var model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

// Relationships
db.Building.hasMany(db.Room);
db.Room.hasMany(db.Booking);
db.Lecturer.hasMany(db.Booking);
db.Subject.hasMany(db.Booking);
db.Slot.hasMany(db.Booking);
db.Booking.belongsTo(db.Subject);
db.Booking.belongsTo(db.Lecturer);
db.Booking.belongsTo(db.Room);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;