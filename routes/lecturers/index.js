var express = require('express');
var router  = express.Router();
var models  = require('../../models');

// Get all lecturer
router.get('/', function(req, res) {
  models.Lecturer.findAll({ include: [{ all: true, nested: true }] })
  .then(function(lecturers) {
    if (lecturers)
      res.json(lecturers);
    else
      res.status(401).send('Lecturers not found');
  });
});

// Get one lecturer information by lecturerid
router.get('/:lecturerid',function(req,res){
  models.Building.findOne(
    {
      where: {id: req.params.lecturerid},
      include: [{ all: true, nested: true }]
    }
  ).then(function(lecturer) {
    if (lecturer)
      res.json(lecturer);
    else
      res.status(401).send('Lecturer not found');
  });
});

module.exports = router;
