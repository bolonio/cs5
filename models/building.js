"use strict";

module.exports = function(sequelize, DataTypes) {
  var Building = sequelize.define("Building", {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    name: DataTypes.STRING,
    description: DataTypes.STRING
  });
  return Building;
};