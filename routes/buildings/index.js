var express = require('express');
var router  = express.Router();
var models  = require('../../models');

// Get all buildings
router.get('/', function(req, res) {
  models.Building.findAll({ include: [{ all: true, nested: true }] })
  .then(function(buildings) {
    if (buildings)
      res.json(buildings);
    else
      res.status(401).send('Buildings not found');
  });
});

// Get one building information by buildingid
router.get('/:buildingid',function(req,res){
  models.Building.findOne(
    {
      where: {id: req.params.buildingid},
      include: [{ all: true, nested: true }]
    }
  ).then(function(building) {
    if (building)
      res.json(building);
    else
      res.status(401).send('Building not found');
  });
});

module.exports = router;
