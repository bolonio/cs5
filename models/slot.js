"use strict";

module.exports = function(sequelize, DataTypes) {
  var Slot = sequelize.define("Slot", {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    name: DataTypes.STRING,
    from: DataTypes.STRING,
    to: DataTypes.STRING
  });
  return Slot;
};