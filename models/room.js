"use strict";

module.exports = function(sequelize, DataTypes) {
  var Room = sequelize.define("Room", {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    //buildingid: DataTypes.INTEGER,
    name: DataTypes.STRING,
    //description: DataTypes.STRING,
    
    Sitzplatze: DataTypes.INTEGER,
    EDV: DataTypes.INTEGER,
    Grosse: DataTypes.INTEGER,
    TelDW: DataTypes.INTEGER,
    Zusatzinfo: DataTypes.STRING,
    
    BodenPVC: DataTypes.INTEGER,
    BodenTeppich: DataTypes.INTEGER,
    Flipchart: DataTypes.INTEGER,
    Magnetleisten: DataTypes.INTEGER,
    Pinnwand: DataTypes.INTEGER,
    TafelWhiteboard: DataTypes.INTEGER,
    TafelWhiteboardBreit: DataTypes.INTEGER,
    TafelWhiteboardMobil: DataTypes.INTEGER,
    TischeFixVerschraubt: DataTypes.INTEGER,
    Beamer: DataTypes.INTEGER,
    Klimaanlage: DataTypes.INTEGER,
    LANAnschlüsseTischen: DataTypes.INTEGER,
    Lautsprecher: DataTypes.INTEGER,
    Medienturm: DataTypes.INTEGER,
    Mikrophon: DataTypes.INTEGER,
    StromanschlüsseTischen: DataTypes.INTEGER,
    Visualizer: DataTypes.INTEGER,
    WLAN: DataTypes.INTEGER
  });
  return Room;
};