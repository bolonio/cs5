var express = require('express');
var router  = express.Router();
var models  = require('../../models');

// Get all rooms
router.get('/', function(req, res) {
  models.Room.findAll({ include: [{ all: true, nested: true }] })
  .then(function(rooms) {
    if (rooms)
      res.json(rooms);
    else
      res.status(401).send('Rooms not found');
  });
});

// Get one room information by roomid
router.get('/:roomid',function(req,res){
  models.Room.findOne(
    {
      where: {id: req.params.roomid},
      include: [{ all: true, nested: true }]
    }
  ).then(function(room) {
    if (room)
      res.json(room);
    else
      res.status(401).send('Room not found');
  });
});

// Get one room information by roomname
router.get('/roomname/:roomname',function(req,res){
  models.Room.findOne(
    {
      where: {name: req.params.roomname},
      include: [{ all: true, nested: true }]
    }
  ).then(function(room) {
    if (room)
      res.json(room);
    else
      res.status(401).send('Room not found');
  });
});

/*

// Get one room by roomid
router.get('/:roomid',function(req,res){
  models.Room.findOne({where: {id: req.params.roomid}}).then(function(room) {
    if (room)
      res.json(room);
    else
      res.status(401).send('room not found');
  });
});

// Delete one room by roomid
router.delete('/:roomid',function(req,res){
  models.Room.findOne({where: {id: req.params.roomid}}).then(function(room) {
    if (room) {
      room.destroy();
      res.status(200).send('room deleted');
    }
    else
      res.status(401).send('room not found');
  });
});

// Update one room by roomid
router.put('/:roomid',function(req,res){
  models.Room.findOne({where: {id: req.params.roomid}}).then(function(room) {
    if (room) {
      room.buildingid = req.body.buildingid;
      room.name = req.body.name;
      //room.description = req.body.description;
      
      room.Sitzplatze = req.body.Sitzplatze;
      room.EDV = req.body.EDV;
      room.Grosse = req.body.Grosse;
      room.TelDW = req.body.TelDW;
      room.Zusatzinfo = req.body.Zusatzinfo;
      
      room.BodenPVC = req.body.BodenPVC;
      room.BodenTeppich = req.body.BodenTeppich;
      room.Flipchart = req.body.Flipchart;
      room.Magnetleisten = req.body.Magnetleisten;
      room.Pinnwand = req.body.Pinnwand;
      room.TafelWhiteboard = req.body.TafelWhiteboard;
      room.TafelWhiteboardBreit = req.body.TafelWhiteboardBreit;
      room.TafelWhiteboardMobil = req.body.TafelWhiteboardMobil;
      room.TischeFixVerschraubt = req.body.TischeFixVerschraubt;
      room.Beamer = req.body.Beamer;
      room.Klimaanlage = req.body.Klimaanlage;
      room.LANAnschlüsseTischen = req.body.LANAnschlüsseTischen;
      room.Lautsprecher = req.body.Lautsprecher;
      room.Medienturm = req.body.Medienturm;
      room.Mikrophon = req.body.Mikrophon;
      room.StromanschlüsseTischen = req.body.StromanschlüsseTischen;
      room.Visualizer = req.body.Visualizer;
      room.WLAN = req.body.WLAN;
      room.save(function(err) {
        if (err)
            res.status(401).send('ERROR: Room not updated');
        else
          res.status(200).send('room updated');
      });
    }
    else
      res.status(401).send('room not found');
  });
});

// Insert one room
router.post('/',function(req,res){
  return models.Room.upsert({
    buildingid: req.body.buildingid,
    name: req.body.name,
    //description: req.body.description,
    
    Sitzplatze: req.body.Sitzplatze,
    EDV: req.body.EDV,
    Grosse: req.body.Grosse,
    TelDW: req.body.TelDW,
    Zusatzinfo: req.body.Zusatzinfo,
    
    BodenPVC: req.body.BodenPVC,
    BodenTeppich: req.body.BodenTeppich,
    Flipchart: req.body.Flipchart,
    Magnetleisten: req.body.Magnetleisten,
    Pinnwand: req.body.Pinnwand,
    TafelWhiteboard: req.body.TafelWhiteboard,
    TafelWhiteboardBreit: req.body.TafelWhiteboardBreit,
    TafelWhiteboardMobil: req.body.TafelWhiteboardMobil,
    TischeFixVerschraubt: req.body.TischeFixVerschraubt,
    Beamer: req.body.Beamer,
    Klimaanlage: req.body.Klimaanlage,
    LANAnschlüsseTischen: req.body.LANAnschlüsseTischen,
    Lautsprecher: req.body.Lautsprecher,
    Medienturm: req.body.Medienturm,
    Mikrophon: req.body.Mikrophon,
    StromanschlüsseTischen: req.body.StromanschlüsseTischen,
    Visualizer: req.body.Visualizer,
    WLAN: req.body.WLAN
  }).then(function(test){
    if(test)
      res.status(200).send('room inserted');
    else
      res.status(401).send('ERROR: Room not inserted');
  });
});
*/
module.exports = router;
