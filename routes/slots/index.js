var express = require('express');
var router  = express.Router();
var models  = require('../../models');

// Get all slots
router.get('/', function(req, res) {
  models.Slot.findAll({ include: [{ all: true, nested: true }], order:['Slot.id'] })
  .then(function(slots) {
    if (slots)
      res.json(slots);
    else
      res.status(401).send('Slots not found');
  });
});

// Get one slot information by slotid
router.get('/:slotid',function(req,res){
  models.Building.findOne(
    {
      where: {id: req.params.slotid},
      include: [{ all: true, nested: true }]
    }
  ).then(function(slot) {
    if (slot)
      res.json(slot);
    else
      res.status(401).send('Slot not found');
  });
});

module.exports = router;
