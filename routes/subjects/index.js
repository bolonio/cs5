var express = require('express');
var router  = express.Router();
var models  = require('../../models');

// Get all subjects
router.get('/', function(req, res) {
  models.Subject.findAll({ include: [{ all: true, nested: true }] })
  .then(function(subjects) {
    if (subjects)
      res.json(subjects);
    else
      res.status(401).send('Subjects not found');
  });
});

// Get one subject information by subjectid
router.get('/:subjectid',function(req,res){
  models.Building.findOne(
    {
      where: {id: req.params.subjectid},
      include: [{ all: true, nested: true }]
    }
  ).then(function(subject) {
    if (subject)
      res.json(subject);
    else
      res.status(401).send('Subject not found');
  });
});

module.exports = router;
