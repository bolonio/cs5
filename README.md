# CS5 REST API

CS5 is a simple open source REST API written in NodeJS (Rest API) and PostgreSQL

## Getting things up and running

### Install the API

- Install [Node.js](http://nodejs.org)
- Install [PostgreSQL](http://www.postgresql.org/)
- Create a new database called 'cs5'
- Adapt the config/config.json file with your username and password in PostgreSQL

```
 $ git clone https://github.com/bolonio/cs5.git
 $ cd cs5
 $ npm install
```

### Run the API

```
 $ npm start
```

This will start the REST API.
Just open [http://localhost:3030](http://localhost:3030).


##License
Copyright (c) 2016 [Adrián Bolonio](https://github.com/bolonio/) Licensed under [the MIT license](https://github.com/bolonio/cs5/blob/master/LICENSE.md).

